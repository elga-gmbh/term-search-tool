/*
 * Copyright (c) 2023 Mag. DI Simon Pirker
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.
 */
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { DataService } from './data.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PhcComponent } from './phc/phc.component';
import { HighlighterPipe } from './highlighter.pipe';
import { Icpc2detailsComponent } from './icpc2details/icpc2details.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { BottombarComponent } from './bottombar/bottombar.component';
import { ImpressumComponent } from './impressum/impressum.component';
import { ZweckbestimmungComponent } from './zweckbestimmung/zweckbestimmung.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
//    PatientsComponent,
//    PatientComponent,
    SidebarComponent,
    PhcComponent,
    HighlighterPipe,
    Icpc2detailsComponent,
    FileUploadComponent,
    BottombarComponent,
    ImpressumComponent,
    ZweckbestimmungComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule

  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
