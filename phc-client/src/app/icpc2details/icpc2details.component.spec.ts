import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Icpc2detailsComponent } from './icpc2details.component';

describe('Icpc2detailsComponent', () => {
  let component: Icpc2detailsComponent;
  let fixture: ComponentFixture<Icpc2detailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Icpc2detailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Icpc2detailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
