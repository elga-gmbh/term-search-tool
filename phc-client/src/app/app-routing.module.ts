/*
 * Copyright (c) 2023 Mag. DI Simon Pirker
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { ImpressumComponent } from './impressum/impressum.component';
import { LoginComponent } from './login/login.component';

import { PhcComponent } from './phc/phc.component';
import { ZweckbestimmungComponent } from './zweckbestimmung/zweckbestimmung.component';

const routes: Routes = [
  {
    path: '',
    component:PhcComponent
  },
  {
    path: 'admin',
    component:FileUploadComponent
  },
  {
    path: 'impressum',
    component:ImpressumComponent
  },
  {
    path: 'zweckbestimmung',
    component:ZweckbestimmungComponent
  },
  {
    path: 'login',
    component:LoginComponent
  },
  {
    path: 'logout',
    component:LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
