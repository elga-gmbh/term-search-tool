/*
 * Copyright (c) 2023 Mag. DI Simon Pirker
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.
 */
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'highlighter'
})

export class HighlighterPipe implements PipeTransform {

  transform( tobesearched: any, args: any, type:string): unknown {
    
    if(!args) return tobesearched;

    var valuesArray: {key:boolean, value:string}[] = [];

    tobesearched.split(" ").forEach ((value:any) => 
      {
        if(value.length>=1){
          valuesArray.push({key: false, value:value});
        }
      }
    )

    var searchterms = args.split(" ");
    for(let i = 0; i< valuesArray.length;i++)
    {
      
      searchterms.forEach((word:string) => {
        if(!valuesArray[i].key && word.length>=1){
          const re = new RegExp(word, 'igm');          
          var replaced = valuesArray[i].value.replace(re, '<span class="highlighted-text">$&</span>');
          if(replaced !== valuesArray[i].value){
            valuesArray[i].key = true;
            valuesArray[i].value = replaced;
          } 
        }

      });
    }
    
    var returnvalue:string = "";
    for(let i = 0; i< valuesArray.length;i++)
    {
      returnvalue = returnvalue + valuesArray[i].value + " ";
      
    };
    return returnvalue;
    
  }
}
