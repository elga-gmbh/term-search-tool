
/*
 * Copyright (c) 2023 Mag. DI Simon Pirker
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.
 */

package at.spi.phc.controller;

import at.spi.phc.model.FileInfo;
import at.spi.phc.model.ICPC2;
import at.spi.phc.model.ResponseMessage;
import at.spi.phc.model.User;
import at.spi.phc.service.ICPC2Service;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.security.Principal;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
public class ICPC2Controller {


    @jakarta.annotation.Resource
    ICPC2Service icpc2Service;


    // Documentation start
    @Operation(summary = "Finds icpc2 entries by keywords in the searchstring",
            description = "Multiple keywords can be transmitted in the searchstring. The searchstring needs to be at least 2 characters and each term also needs to be at least 2 characters in length. There is a limit of 4 searchterms in the searchstring.",tags = "search")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "A List of all the ICPC2 value sets found in the Thesaurus",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ICPC2.class))})})
    // Documentation end
    @GetMapping("/api/icpc2/search")
    public @ResponseBody List<ICPC2> getByTerm(
            @Parameter(description = "Values to be searched in the ICPC2 Description and in the ICPC2 Code itself (e.g. \"A01\" or \"Grip Inf\")")
            @RequestParam(value = "term", required = false) String searchTerm,

            @Parameter(description = "Which code System should be searched. Options: ICPC2, ICPC3, SNOMEDCT, ICD10")
            @RequestParam(value = "codeSystem", required = false) String codeSystem,
            @Parameter(description = "Which code System should be searched. Options: ICPC2, ICPC3, SNOMEDCT, ICD10")
            @RequestParam(value = "kapitel", required = false) String kapitel,
            @Parameter(description = "Which code System should be searched. Options: ICPC2, ICPC3, SNOMEDCT, ICD10")
            @RequestParam(value = "color", required = false) Integer farbe){
        return icpc2Service.getByTerm(searchTerm, codeSystem, kapitel, farbe);

    }

    // Documentation start
    @Operation(summary = "Upload a version of the ICPC2 data table",
            description = "The file will be saved on the server and listed for activation or download." ,tags = "file")

    // Documentation end
    @Secured("ROLE_ADMIN")
    @PostMapping("/api/sec/upload")
    public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file) {
        String message = "";
        try {
            icpc2Service.saveFile(file);

            message = "Uploaded the file successfully: " + file.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
        } catch (Exception e) {
            message = "Could not upload the file: " + file.getOriginalFilename() + ". Error: " + e.getMessage();
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new ResponseMessage(message));
        }
    }

    // Documentation start
    @Operation(summary = "Select one of the uploaded files to be loaded into the searchtool. ",
            description = "The search is being performed against the loaded file." ,tags = "file")
    // Documentation end
    @Secured("ROLE_ADMIN")
    @PostMapping("/api/sec/activatefile")
    public ResponseEntity<ResponseMessage> activateFile(
            @Parameter(description = "The name of the file to be activated for search within the searchtool. Failed lines within the import are being returned in te response body.")
            @RequestParam("file") String file) {
        String message = "";
        try {
            System.out.println("Activate File " + file);
            String failedLines = icpc2Service.readCsvICPC2(file);
            System.out.println("Failed Lines: " + file);
            message = "Datei erfolgreich geladen: " + file + " Fehlgeschlagene Zeilen: " + failedLines;
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
        } catch (Exception e) {
            message = "Datei konnte nicht hochgeladen werden: " + file + ". Fehler: " + e.getMessage();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
        }
    }

    // Documentation start
    @Operation(summary = "Get a list of all files, that were uploaded to the server. ",
            description = "The files are different versions of the ICPC2 Catalog in csv utf8 format. Replace all existing ; with § within the data first." ,tags = "file")
    // Documentation end
    //@Secured("ROLE_ADMIN")
    @GetMapping("/api/icpc2/files")
    public ResponseEntity<List<FileInfo>> getListFiles() {
        List<FileInfo> fileInfos = icpc2Service.loadAll().map(path -> {
            String filename = path.getFileName().toString();
            String url = MvcUriComponentsBuilder
                    .fromMethodName(ICPC2Controller.class, "getFile", path.getFileName().toString()).build().toString();

            return new FileInfo(filename, url);
        }).collect(Collectors.toList());

        return ResponseEntity.status(HttpStatus.OK).body(fileInfos);
    }
    // Documentation start
    @Operation(summary = "Download a specific file from the server.",
            description = "The filename must be one of the ones that were uploaded and can be fetched via /api/icpc2/files" ,tags = "file")
    // Documentation end
    //@Secured("ROLE_ADMIN")
    @GetMapping("/api/icpc2/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(
            @Parameter(description = "The name of the file to be activated for search within the searchtool. Failed lines within the import are being returned in te response body.")
            @PathVariable String filename) {
        Resource file = icpc2Service.load(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    //TODO: https://www.baeldung.com/spring-security-login-angular

    @RequestMapping("/api/icpc2/login")
    public boolean login(@RequestBody User user) {
        return
                user.getUserName().equals("user") && user.getPassword().equals("password");
    }

    @RequestMapping("/api/icpc2/user")
    public Principal user(HttpServletRequest request) {
        String authToken = request.getHeader("Authorization")
                .substring("Basic".length()).trim();
        return () ->  new String(Base64.getDecoder()
                .decode(authToken)).split(":")[0];
    }


}
