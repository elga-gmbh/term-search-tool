/*
 * Copyright (c) 2023 Mag. DI Simon Pirker
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.
 */
package at.spi.phc.service;

import at.spi.phc.model.ICPC2;
//import at.spi.phc.repository.ICPC2Repository;
import jakarta.annotation.PostConstruct;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
//import org.springframework.data.domain.PageRequest;
//mport org.springframework.data.domain.Pageable;
//import org.springframework.data.domain.Slice;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.nio.file.*;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

@Service
public class ICPC2Service {

    private static final String filename ="classpath:data/KL-OEGAM-PrimaryCareCode_R1.csv";

    private static final int matchWeight = 4;
    private static final int variationWeight = 3;

    private static final int FIELD_ID = 4;
    private static final int FIELD_BEGRIFFE= 3;
    private static final int FIELD_KRITERIEN= 5;
    private static final int FIELD_REDFLAGS = 6;
    private static final int FIELD_DIFFERENTIAL = 7;
    private static final int FIELD_HINWEISE = 8;
    private static final int FIELD_KOMPONENTE = 9;
    private static final int FIELD_FARBE = 10;
    private static final int FIELD_SNOMEDCT = 1;
    private static final int FIELD_FSNSNOBEDCT= 2;
    private static final int FIELD_SUBSTANZCODE = 11;
    private static final int FIELD_ICPC3 = 12;
    private static final int FIELD_ICD10DIMDI = 13;
    private static final int FIELD_ICD10DISPLAY= 14;
    private static final int FIELD_ICD11 = 15;
    private static final int FIELD_ICD11BEGRIFFE= 16;


    private Path root;

    @Autowired
    ResourceLoader resourceLoader;

    Dictionary<String, ICPC2> icpc2Cache = new Hashtable<>();
    @PostConstruct
    public void postConstruct() {
        root = Paths.get("uploads");
        createCache();
        init();
    }

    public void init() {
        try {
            Files.createDirectories(root);
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize folder for upload!");
        }
    }

    public void saveFile(MultipartFile file) {

        try {
            Files.copy(file.getInputStream(), this.root.resolve(file.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            if (e instanceof FileAlreadyExistsException) {
                throw new RuntimeException("A file of that name already exists.");
            }

            throw new RuntimeException(e.getMessage());
        }
    }

    public Resource load(String filename) {
        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.root, 1).filter(path -> !path.equals(this.root)).map(this.root::relativize);
        } catch (IOException e) {
            throw new RuntimeException("Could not load the files!"+e.getMessage());
        }
    }


    private void createCache(){

        icpc2Cache = new Hashtable<>();
        readCsvICPC2("");
    }


    public List<ICPC2> getByTerm(String searchTerm, String codeSystem, String kapitel, int farbe) {
        Instant start = Instant.now();
        List<ICPC2> returnList = new ArrayList<>();
        MultiValuedMap<Integer, ICPC2> multimap = new HashSetValuedHashMap<>();

        int delimiter = 200;

        //allgemein
        if(searchTerm != null && !searchTerm.isEmpty() && searchTerm.length()>1) {


            //Filter Words with less than 2 letters
            // and only 4 words maximum search lengt
            List<String> searchTerms = Arrays.stream((searchTerm.split("\\s+")))
                    .filter(s -> s.length() >= 2)
                    .limit(4)
                    .toList();



            for (String currentKey : Collections.list(icpc2Cache.keys())) {

                int matchesWeight = 0;
                int matchesCount = 0;


                for (String s : searchTerms) {
                    //Skip term, if not at least 2 characters or more than 4 terms


                    //Find searchterm itself
                    int searchWeight = 0;
                    searchWeight+= findMatches(s, currentKey, true, codeSystem, kapitel, farbe)*matchWeight;
                    //Variations of term
                    if(searchWeight ==0) {
                        searchWeight += findVariations(s, currentKey, false, "k", "c", codeSystem, kapitel,farbe) * variationWeight;
                    }
                    if(searchWeight ==0) {
                        searchWeight += findVariations(s, currentKey, false, "c", "k", codeSystem, kapitel,farbe) * variationWeight;
                    }
                    if(searchWeight ==0) {
                        searchWeight += findVariations(s, currentKey, false, "c", "z", codeSystem, kapitel,farbe) * variationWeight;
                    }
                    if(searchWeight ==0) {
                        searchWeight += findVariations(s, currentKey, false, "z", "c", codeSystem, kapitel,farbe) * variationWeight;
                    }
                    if(searchWeight ==0) {
                        searchWeight += findVariations(s, currentKey, false, "z", "ck", codeSystem, kapitel,farbe) * variationWeight;
                    }
                    if(searchWeight  >0){matchesCount++;}
                    matchesWeight += searchWeight;


                }


                //Only show results with at least n Matches, if n search Terms are entered
                if (matchesCount >= searchTerms.size()) {

                    multimap.put(matchesWeight, icpc2Cache.get(currentKey));
                }
            }



            for (Map.Entry<Integer, ICPC2> entry:multimap.entries().stream().sorted(Comparator.comparing(Map.Entry<Integer, ICPC2>::getKey).reversed()).toList()
            ) {
                returnList.add(entry.getValue());
            }

        }
        else{
            //Return all Elements
            Predicate<ICPC2> kapitelPredicate = item -> (kapitel.length()>0)? kapitel.equals(item.icpc2.substring(0,1)) : true;
            Predicate<ICPC2> farbePredicate = item -> (farbe > 0)?farbe == item.farbe : true;
            returnList = Collections.list(icpc2Cache.elements()).stream()
                    .filter(kapitelPredicate)
                    .filter(farbePredicate)
                    .sorted(Comparator.comparing(i2 -> i2.icpc2))
                    .toList();
        }

        returnList.stream().sorted(Comparator.comparing(i -> i.icpc2));
        System.out.println("Returnlist size "+ returnList.size());
        Instant end = Instant.now();
        Duration duration = Duration.between(start, end);
        System.out.println("Duration "+duration.getSeconds() + " - " + duration.getNano());

        return returnList.subList(0,Math.min(returnList.size(),delimiter));


    }

    private int findVariations(String s,  String currentKey, boolean includeCode, String original,String variation, String codeSystem, String kapitel, int color) {
        int matches = 0;
        int index = s.indexOf(original);
        while (index >= 0) {
            matches += findMatches(s.substring(0, index) + variation + s.substring(index + 1),currentKey,includeCode, codeSystem, kapitel, color);
            index = s.indexOf(original, index + 1);
        }
        return matches;
    }

    private int findMatches(String s, String currentKey, boolean includeCode, String codeSystem, String kapitel, int farbe) {
        Pattern p = Pattern.compile(Pattern.quote(s), Pattern.CASE_INSENSITIVE);
        Pattern pId = Pattern.compile("^"+Pattern.quote(s), Pattern.CASE_INSENSITIVE);
        try {
            String currentBegriff = icpc2Cache.get(currentKey).begriffe;
            int currentFarbe = icpc2Cache.get(currentKey).farbe;
            String currentKapitel = ""+icpc2Cache.get(currentKey).icpc2.charAt(0);
            //Filter Farbe
            if(farbe >0 && !(farbe == currentFarbe)){
                return 0;
            }
            //Filter Kapitel

            if(kapitel.length() >= 1 && !(kapitel.equals(currentKapitel))){
                return 0;
            }
            String currentID = "";
            switch (codeSystem) {
                case "ICPC2":
                    currentID = icpc2Cache.get(currentKey).icpc2;
                    break;
                case "ICPC3":
                    currentID = icpc2Cache.get(currentKey).icpc3;
                    break;
                case "SNOMEDCT":
                    currentID = icpc2Cache.get(currentKey).snomedCT;
                    break;
                case "ICD10":
                    currentID = icpc2Cache.get(currentKey).icd10;
                    break;
                default:
                    currentID = icpc2Cache.get(currentKey).icpc2;
            }

            if ( p.matcher(currentBegriff).find()) {
                //If Match in a single-word Begriff, priorize result with best similarity = shortest word
                if(currentBegriff.indexOf(' ') == -1) {
                    //System.out.println("result match "+ " " + s + " " + currentBegriff + " "+ (s.length()*3-currentBegriff.length()));
                    return s.length()*100-currentBegriff.length();
                }
                else {
                    return 1;
                }
            }
            else if (includeCode && pId.matcher(currentID).find()) {
                return 1;
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        return 0;
    }


    public String readCsvICPC2(String filename) {
        boolean oneSuccessfulImport = false;
        String line = "";
        String splitBy = ";";

        String failedLines = "";
        try
        {
            Resource res = null;
            if(!filename.isEmpty()){
                try {
                    Path file = root.resolve(filename);
                    Resource resource = new UrlResource(file.toUri());
                    if (resource.exists() || resource.isReadable()) {
                        res = resource;
                    } else {
                        throw new RuntimeException("Could not read the file!");
                    }
                } catch (MalformedURLException e) {
                    throw new RuntimeException("Error: " + e.getMessage());
                }
            }
            else {
                res = resourceLoader.getResource("classpath:data/KL-OEGAM-PrimaryCareCodeR3_6.csv");
            }
            InputStream inputStream = res.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            line = br.readLine();


            int id = 0;


            while ((line = br.readLine()) != null)   //returns a Boolean value
            {

                if(line.isEmpty()){
                    continue;
                }
                try {

                    String[] icpc2 = line.split(splitBy, -1);    // use comma as separator

                    if((icpc2.length <3) || (!StringUtils.hasText(icpc2[FIELD_ID])) || (icpc2[FIELD_ID].length() < 3))
                    {
                        System.out.println("Skipped" + line + " "+icpc2.length+ " " +(icpc2.length <3) + " "+ !StringUtils.hasText(icpc2[FIELD_ID])+ " "+ (icpc2[FIELD_ID].length() < 3));
                        continue;
                    }

                    ICPC2 datensatz = ICPC2.builder()
                            .id(icpc2[FIELD_ID]+"."+id)
                            .icpc2(icpc2[FIELD_ID])
                            .begriffe(icpc2[FIELD_BEGRIFFE].trim())
                            .kriterien(icpc2.length >4 ? icpc2[FIELD_KRITERIEN]:"")
                            .redFlags(icpc2.length >5 ? icpc2[FIELD_REDFLAGS]:"")
                            .differentialdiagnosen(icpc2.length >6 ? icpc2[FIELD_DIFFERENTIAL]:"")
                            .hinweise(icpc2.length >7 ? icpc2[FIELD_HINWEISE]:"")
                            .komponente(icpc2.length >8 && icpc2[FIELD_KOMPONENTE].length() > 0 ? Integer.parseInt(icpc2[FIELD_KOMPONENTE]):0)
                            .farbe(icpc2.length >9 && icpc2[FIELD_FARBE].length() > 0 ? Integer.parseInt(icpc2[FIELD_FARBE]):0)
                            .snomedCT(icpc2.length >10 ? icpc2[FIELD_SNOMEDCT]:"")
                            .snomedCTInternationalBrowser(icpc2.length >11 ? icpc2[FIELD_FSNSNOBEDCT]:"")
                            .substanzcode(icpc2.length >12 ? icpc2[FIELD_SUBSTANZCODE]:"")
                            .icpc3(icpc2.length >13 ? icpc2[FIELD_ICPC3]:"")
                            .icd10(icpc2.length >14 ? icpc2[FIELD_ICD10DIMDI]:"")
                            .icd10Display(icpc2.length >15 ? icpc2[FIELD_ICD10DISPLAY]:"")
                            .icd11(icpc2.length >16 ? icpc2[FIELD_ICD11]:"")
                            .icd11Begriffe(icpc2.length >17 ? icpc2[FIELD_ICD11BEGRIFFE]:"")

                            .build();



                    if (icpc2[3].length() > 0) {
                        Pattern MY_PATTERN = Pattern.compile("([A-Z][0-9][0-9])");
                        Matcher m = MY_PATTERN.matcher(icpc2[3]);
                        int lastIndex = 0;



                    }
                    //Only clear Cache if there is at least one line read successfully
                    if(oneSuccessfulImport == false)
                    {
                        oneSuccessfulImport = true;
                        icpc2Cache = new Hashtable<>();

                    }
                    icpc2Cache.put(Integer.toString(id),datensatz);
                    id++;


                }

                catch (Exception e)
                {
                    System.out.println("Failed line: "+line );
                    failedLines += line + " // ";
                    e.printStackTrace();
                }

            }
            System.out.println("Imported" + id + " lines from File: "+filename );
        }
        catch (Exception e)
        {
            System.out.println("Failed line: "+line);

            e.printStackTrace();
        }

        return failedLines;
    }
}
