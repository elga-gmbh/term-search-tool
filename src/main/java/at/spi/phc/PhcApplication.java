/*
 * Copyright (c) 2023 Mag. DI Simon Pirker
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.
 */
package at.spi.phc;


import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.servers.Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@EnableMethodSecurity(securedEnabled = true)
//Documentation start
@OpenAPIDefinition(info = @Info(
		termsOfService = "https://oegam.at/icpc-2",
		license = @License(url = "https://www.gnu.org/licenses/lgpl-3.0.txt", name= "GPLv3"),
		contact = @Contact(email = "pirker (at) spengergasse (dot) at"),
		title = "Search Tool KL-OEGAM",
		version = "1.0",
		description = "The Search Tool KL-OEGAM offers an API to search for matches in the ICPC2 Thesaurus. It is supposed to offer easy search but no decision support and is not designed as a medical product. \n" +
				"    The API is open for general practitioners within Austria and an open source project of ÖGAM (https://oegam.at/)"),
		servers = {@Server(url = "130.61.243.144:8080", description = "Default Server URL")}
	)
//Documentation end
public class PhcApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhcApplication.class, args);
	}
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("*").allowedOrigins("*");
			}
		};
	}


	@Bean
	public PasswordEncoder passwordEncoder(){

		return new BCryptPasswordEncoder(15);

	}



}
