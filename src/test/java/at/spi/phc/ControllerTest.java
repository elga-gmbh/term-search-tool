/*
 * Copyright (c) 2023 Mag. DI Simon Pirker
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.
 */
package at.spi.phc;

import at.spi.phc.model.ICPC2;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
//import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest

@AutoConfigureMockMvc
public class ControllerTest {
    @Autowired
    MockMvc mvc;

    @Autowired
    ObjectMapper om;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Test
    public void
    getAllICPC2s() {
        try {
            mvc
                    .perform(MockMvcRequestBuilders.get("/api/icpc2/search?allgemein=Karzi"))
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(status().isOk())
                    .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].icpc2").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].begriffe").exists())

                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].kriterien").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].redFlags").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].differentialdiagnosen").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].hinweise").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].komponente").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].farbe").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].snomedCT").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].snomedCTInternationalBrowser").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].substanzcode").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].icpc3").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].icd10").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].icd10Display").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].icd11").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].icd11Begriffe").exists())

            ;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    @WithAnonymousUser
    public void whenAnonymousAccessLogin_thenNotAllowed() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/sec/upload"))
                .andExpect(status().isMethodNotAllowed());
    }



    @Test
    @WithUserDetails("OEGAM")
    public void fileUploadWithAuthentication()
            throws Exception {
        MockMultipartFile file
                = new MockMultipartFile(
                "file",
                "hello.txt",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );

        MockMvc mockMvc
                = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        mockMvc.perform(multipart("/api/sec/upload").file(file))
                .andExpect(status().isOk());
    }

}

